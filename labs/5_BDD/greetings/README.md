# User Story

**As a** good fellow worker
**I want** to say hello to anyone in the office
**So that** everyone can start the day with good mood
  
# BDD (focus on what?)

We will start by capturing the **what?** (the behaviour)

## The boostrap

For once we will start by adding test scoped maven dependencies for cucumber.

Create a maven project with 

* groupId = "com.acme" 
* artifactId = "greetings"

Add to the `pom.xml`:
 
* the following properties
  ```xml
  <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
  <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
  <java.version>1.8</java.version>
  ```
* the dependencies for 
    - **cucumber-java8**
    - **cucumber-junit** 

## First iteration

### First feature

In **src/test/resources/features**, create a file `greetings.feature` like this:

```gherkin
Feature:
  As a good fellow worker
  I want to say hello to anyone in the office
  So that everyone can start the day with good mood

  Scenario: Greet John
    When I want to greet John
    Then System should says: 'Hello John'
```


Under **Feature** comes a free text description. It is quite common

### First step definition

Add to the **pom.xml** the following dependencies with test scope:

* junit:junit.4.12 
* org.springframework.spring-web:4.3.3.RELEASE


In **src/test/java**, create the package `com.acme.greetings` the add the two following classes:
 
```java
@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/features"
)
public class FeaturesTest {
}
```

```java
public class GreetingsSteps {

    private String message;

    @Given("^I want to greet John$")
    public void greetJohn() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
        message = restTemplate.getForObject("http://localhost:8080/api/greetings?name=John", String.class);
    }

    @Then("^System should says: Hello John$")
    public void sayHelloJohn() {
        assertEquals("Hello John", message);
    }
}
```


Remarks:

* As you can see, according to the [documentation](https://cucumber.io/docs/reference/jvm#java), test and step definitions stand in two different classes.
* Steps classes in general will have a mutable state shared between step definitions (here it's `private String message;`) this is completly unusual from unit testing perspective but pretty common from Cucumber perspective.

Run the test: it fails so let's implement something.

### First (naive) implementation

#### Boostrap

Add the `pom.xml` the following:

```xml
    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>1.4.1.RELEASE</version>
        <relativePath/>
    </parent>
```

```xml
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
```

**Refactoring:**

Because **spring-boot-starter-web** contains all the necessary dependencies for web you can remove **spring-web** we had previously.

#### One class

To **src/main/java**, in package `com.acme.greetings` add the following class:

```java
@SpringBootApplication
@RestController
public class GreetingsApplication {

    public static void main(String[] args) {
        SpringApplication.run(GreetingsApplication.class, args);
    }

    @GetMapping(value = "/api/greetings", produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> sayHello(@RequestParam(required = false) String name) {
        return ResponseEntity.ok("Hello " + name));
    }

}
```

Start the app and run the test to see it succeed.

## Second round

### A new scenario

Opend file `greetings.feature` to add the new following scenario.

```gherkin
  Scenario:
    When I want to greet everyone
    Then System should says: 'Hello World'
```

If you run the test you will see that previous scenario keep on runnning and test succeeds.
As a warning if also mention that you have missing steps (so our new scenario does not run)

**You can implement missing steps with the snippets below:**

```
When("^I want to greet everyone$", () -> {
    // Write code here that turns the phrase above into concrete actions
    throw new PendingException();
});

Then("^System should says: 'Hello World'$", () -> {
    // Write code here that turns the phrase above into concrete actions
    throw new PendingException();
});
```

(Note that this is java8 closure code)
The default implementation throws a `PendingException` which ends up with step in pending status (=> test keep on not failing).

**Remarks**
You have strict mode you can enable from the annation **`@CucumberOptions`** if you want your test to fail in case of missing steps.

### Refactor scenarios into scenario outline

We can see here that both scenario are the same except for their parameters.
It's the perfect occasion to refactor the two into [Scenario Outline](https://github.com/cucumber/cucumber/wiki/Scenario-Outlines)

```gherkin
Feature:
  As a good fellow worker
  I want to say hello to anyone in the office
  So that everyone can start the day with good mood

  Scenario Outline: Greet someone in particular or everyone in general
    When I want to greet <name>
    Then System should display: <message>

    Examples:
      | name     | message     |
      | John     | Hello John  |
      | everyone | Hello World |
```

### Step definitions

Let's refactor this to java8 as proposed by the test run, `GreetingsSteps` becomes:

```java
pulic class GreetingsSteps implements En {

    private String message;

    public GreetingsSteps() {
        When("^I want to greet (\\w*)$", (String name) -> {
            final String querystring = "everyone".equals(name) ? "" : "?name=".concat(name);
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
            message = restTemplate.getForObject("http://localhost:8080/api/greetings".concat(querystring), String.class);
        });

        Then("^System should display: (\\w* \\w*)$", (String message) -> {
            assertEquals(message, this.message);
        });
    }

}
```

## Moving forward

### Generating reports

To the annotation `@CucumberOptions` in class `FeaturesTest` add `plugin = {"pretty", "html:target/cucumber/html",  "json:target/cucumber/cucumber.json"}`.
Then run the test, you are now able to see:

 * a pretty **console** display
 * an html report under **target/cucumber/html**
 * a json report in **target/cucumber/cucumber.hson**

### Make it part of the build

To the `pom.xml` add the following to build section:

```xml
        <plugins>
            <plugin>
                <artifactId>maven-surefire-plugin</artifactId>
                <configuration>
                    <excludes>
                        <exclude>**/FeaturesTest.java</exclude>
                    </excludes>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-failsafe-plugin</artifactId>
                <configuration>
                    <includes>
                        <include>**/FeaturesTest.java</include>
                    </includes>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <executions>
                    <execution>
                        <id>pre-integration-test</id>
                        <goals>
                            <goal>start</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>post-integration-test</id>
                        <goals>
                            <goal>stop</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
```

# TDD (focus on how?)

We may think about refactoring for a cleaner design. So it's time to use TDD to capture the **how?**