package com.acme.greetings;

import cucumber.api.java8.En;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.assertEquals;


/**
 * Created by rguitter on 04/11/2016.
 */
public class ByeSteps implements En {

    private String message;

    public ByeSteps() {
        When("^I want to bye bye (\\w*)$", (String name) -> {
            final String querystring = "everyone".equals(name) ? "" : "?name=".concat(name);
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
            message = restTemplate.getForObject("http://localhost:8080/api/bye".concat(querystring), String.class);
        });

        Then("^System should say: (\\w* \\w*)$", (String message) -> {
            assertEquals(message, this.message);
        });
    }

}
