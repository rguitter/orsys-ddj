package com.acme.greetings;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by rguitter on 04/11/2016.
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/features",
        plugin = {"pretty", "html:target/cucumber/html", "json:target/cucumber/cucumber.json"}
)
public class FeaturesTest {
}
