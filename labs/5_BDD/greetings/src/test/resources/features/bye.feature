Feature:
  As a good fellow worker
  I want to say bye to anyone in the office
  So that everyone can end the day with good mood

  Scenario Outline: Bye bye someone in particular or everyone in general
    When I want to bye bye <name>
    Then System should say: <message>

    Examples:
      | name     | message     |
      | John     | Bye John  |
      | everyone | Bye World |
