Feature:
  As a good fellow worker
  I want to say hello to anyone in the office
  So that everyone can start the day with good mood

  Scenario Outline: Greet someone in particular or everyone in general
    When I want to greet <name>
    Then System should display: <message>

    Examples:
      | name     | message     |
      | John     | Hello John  |
      | everyone | Hello World |
