# Industrialization

For this we will use a simple HelloWorld project.
We refactor it be extracting all the dependencies relative to the test into a BOM and the use it in the project.
Then we will configure its build in our Software factory

## Maven

### Installation

* download the latest version for the [site](https://maven.apache.org/)
* unzip and create environment variable **M2_HOME** that point the installation directory
* amend the environment variable **Path** by prepending it with `%M2_HOME\bin;`
* verify the installation by running form the commande line `mvn --version`

### Create a BOM

**Some readings**

* [BOM explained by Xebia](http://blog.xebia.fr/2014/02/28/la-notion-de-bom-avec-maven/)
* [The BOMs from spring boot](http://docs.spring.io/spring-boot/docs/current/reference/html/using-boot-build-systems.html)

## Software factory

### Installation

To ease the installation we will use docker.
Through we will get:

* gitbucket (a github on premise), 
* jenkins (for the CI, no plugins to visialize reports) 
* sonar (for report).

### Git

* Create a local reposiroty by going in command line to the directory of HelloWorld then run `git init`.
* Create a Git repository on [GitBucket](http://localhost/gitbucket) (you can connect as root/root).
* The run `git remote set-url origin [URL of the repo]`.
* Finally push your version to the remote repository.

### Creation of a BuilAndUnitTest job

Open your browser to go to jenkins (you can connect as root/root)
From the wizard create a "free style job" and configure git URL and mvn options (for example: clean verify)
