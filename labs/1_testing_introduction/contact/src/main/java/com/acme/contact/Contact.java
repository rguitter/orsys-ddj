package com.acme.contact;

/**
 * Created by rguitter on 07/11/2016.
 */
public class Contact {

    private String firstname, lastname, alias, mail;

    public Contact(String firstname, String lastname, String alias, String mail) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.alias = alias;
        this.mail = mail;
    }
}
