package com.acme.contact;

/**
 * Created by rguitter on 27/10/2016.
 */
public abstract class ContactServiceTest {

    private ContactService sut;

    public void invalidContact() {
        // Given
        final int countDb = countContact();

        // When
        sut.save(null);

        // Then
        checkException(NullPointerException.class);
        assert (countDb == countContact());
    }

    public void duplicateAlias() {

        // Given
        setupDb(new Contact("John", "Doe", "jdoe", "jdoe@gmail.com"));
        final int countDb = countContact();

        // When
        try {
            sut.save(new Contact("Jack", "Daniels", "jdoe", "jdaniels@gmail.com"));
            assert(false);
        } catch (DuplicationAliasException e) {
            assert(true);
        }

        // Then
        assert (countDb == countContact());
    }


    public void ok() {

        // Given
        final int countDb = countContact();

        // When
        sut.save(new Contact("Jack", "Daniels", "jdoe", ""));

        // Then
        assert (countDb+1 == countContact());

    }

    abstract int countContact();

    abstract void setupDb(Contact... contacts);

    abstract Contact getByAlias(String alias);

    abstract <T extends Exception> Boolean checkException(Class<T> exceptionClass);

}
