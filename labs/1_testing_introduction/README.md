# Use case

Let's start with a small game.

Consider the following use case:
Saving **Contact** for an email service

* **Contact** has a firstname, a lastname, an email address and an alias which has to be unique.
* the alias is provided by the user, so he may refer to a **Contact** through its alias (imagine an autocompletion field using the alias as for outlook)

We will focus on the servie which allows a user to save a contact. By saving we mean creating and updating.

# Introduction to BDD

Define in natural language (forget about the code) the test cases you want to perform to validate the "saving contact" service.

Expected output at the end: You should end up by crafting your own test description language.

This would illustrate one of the possible way to express structure test in natural language (which is the first step for BDD)

# Introduction to TDD

## First though

Write down on post it the following primitives:

* setUpDb (insert the contacts provided as parameter into the DB)
* countContact (return the number of contacts in the DB)
* getByAlias (return if exists the contact identified by the alias)
* checkException (return true if the given exception has been raised)

Define with the following test primitives, the test cases you want to perform to validate the "saving contract" service.

## Let's go for some code

Create a maven project (groupId=com.acme, artifactId=contact, version=1.0-SNAPSHOT)

### Compile some test

Create the following class `com.acme.contact.ContactServiceTest` in `src/test/java`
and copy the following snippet to the test class:

```java
    abstract int countContact();

    abstract void setupDb(Contact... contacts);

    abstract Contact getByAlias(String alias);

    abstract Boolean checkException(Class<Exception> exceptionClass);
```

Now make it compile by implementing `com.acme.contact.Contact` in `src/main/java``

Implement the tests you have defined wiht postit.  

Expected output: A class `com.acme.contact.ContactService` and the signature(s) of the method(s) to be called for saving a contact.

As you can see we are thinking the test without figuring out any implementation detail:
By considering the primitives as absract, we are able to define what we want to test witout diving directly to implementation: "We are separating the **What?** from the **How?**";

### Let run some test

In order to run the test we need at least to implement the abstract methods in the test class.
Then this will reveal the design of the implementation:

* For now we have some method signature
* Now it's time to decide if we have a DAO or not
* We can decide to implement the DAO so we have write test first ...

We won't go to the end, the idea is just to illustrate how we will use test to drive the design 
(wich is ill the while point of the TDD methodology).

# Question

What seems the most natural for you to start with: BDD or TDD ?