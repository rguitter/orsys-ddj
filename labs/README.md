# Agenda

* Introduction TDD/ATDD
* Automated test (code)
* TDD
* Automated test (acceptance)
* ATDD/BDD
* Industrialization

# Environment setup:

* [Jdk 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* Maven [3.3](http://maven.apache.org/download.cgi)
* IDE:
  - IntelliJ IDEA [Ultimate](http://www.jetbrains.com/idea/download/)
  - Eclipse [Mars](http://www.eclipse.org/downloads/packages/eclipse-ide-java-ee-developers/neon1a)


# Industrialization

[BOM explain by Xebia](http://blog.xebia.fr/2014/02/28/la-notion-de-bom-avec-maven/)
[Les BOM de spring boot](http://docs.spring.io/spring-boot/docs/current/reference/html/using-boot-build-systems.html)

# Automated test

A (quite) complete overview:

* Unit test
    - solitary
    - sociable
* Integration test
* Functional test
    - GUI (web based)
    - No UI (web service based)
* Other test (performance, ...)

Some exotism:

* Mutant testing
* Theory (aka property) based testing

## Automated test

What about tooling to support the common structure of a test:

* Initialisation
* Action
* Verfication

[junit](http://junit.org/junit4/)
[testng](http://testng.org/doc/index.html)

Get it from maven.

Tooling
Maven, jenkins, sonar

## Unit

The two flavors: solitary vs sociable.
Explanation [here](http://martinfowler.com/bliki/UnitTest.html) and [here](https://gist.github.com/jaycfields/eeddc2ead3fee059cce1)

### Solitary

**Question:** How to deal with the isolation ?
**Answer:** Mock and stub

**Tool**
[Mockito](http://mockito.org/)

### sociable

**Question:** How do we load collaborators ?
**Answer:** DI to the rescue.

## Exotism

### Theory (aka property based) testing

tools:

* [https://github.com/NCR-CoDE/QuickTheories](https://github.com/NCR-CoDE/QuickTheories)
* [http://pholser.github.io/junit-quickcheck/site/0.6.1/background/similar-projects.html](http://pholser.github.io/junit-quickcheck/site/0.6.1/background/similar-projects.html)

articles:

* [1](http://www.ontestautomation.com/an-introduction-to-property-based-testing-with-junit-quickcheck/)
* [2](https://blog.sourceclear.com/property-based-testing-for-java/)

# TDD

Same as unit test, but write it first :)

# ATDD

Same as functional test, but wirte it first :)

Slides:

* [https://docs.google.com/presentation/d/1wnS6TbqkKRFkEQ7tmcKZ5SxwFoJeJc1ljT_BLZe7o2o/edit#slide=id.gace778f3_135](https://docs.google.com/presentation/d/1wnS6TbqkKRFkEQ7tmcKZ5SxwFoJeJc1ljT_BLZe7o2o/edit#slide=id.gace778f3_135)
* [http://fr.slideshare.net/nashjain/acceptance-test-driven-development-350264](http://fr.slideshare.net/nashjain/acceptance-test-driven-development-350264)