package com.acme.authenticator;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;

/**
 * Created by rguitter on 07/11/2016.
 */
public class ClearTextPasswordEncoderTest {

    private ClearTextPasswordEncoder sut = new ClearTextPasswordEncoder();

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void encode_KO() throws Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Can't encode a null password.");
        sut.encode(null);
    }

    @Test
    public void encode_OK() throws Exception {
        final String password = "toto";
        assertEquals(password,sut.encode(password));
    }
}
