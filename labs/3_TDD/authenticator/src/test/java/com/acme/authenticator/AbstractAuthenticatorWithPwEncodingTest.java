package com.acme.authenticator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;

/**
 * Created by rguitter on 08/11/2016.
 */
@RunWith(SpringRunner.class)
public abstract class AbstractAuthenticatorWithPwEncodingTest {

    protected AbstractAuthenticatorWithPwEncodingTest(String password) {
        this.password = password;
    }

    @Configuration
    @ComponentScan
    public static class TestConfig {
        @Bean
        public UserAccountDao userAccountDao() {
            return Mockito.mock(UserAccountDao.class);
        }
    }

    @Autowired
    private UserAccountDao userAccountDao;

    @Autowired
    private Authenticator sut;

    private final String password;

    @Test
    public void authenticate_OK() {
        given(userAccountDao.getByLogin("jdoe")).willReturn(new UserAccount("jdoe", password));
        assertTrue(sut.authenticate("jdoe", "jdoepw"));
    }
}
