package com.acme.authenticator;

import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

/**
 * Created by rguitter on 08/11/2016.
 */
@ActiveProfiles("CLEAR")
@ContextConfiguration(classes = AbstractAuthenticatorWithPwEncodingTest.TestConfig.class)
public class AuthenticatorWitClearPwEncodingTest extends AbstractAuthenticatorWithPwEncodingTest {
    public AuthenticatorWitClearPwEncodingTest() {
        super("jdoepw");
    }
}
