package com.acme.authenticator;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Date;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

/**
 * Created by rguitter on 07/11/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthenticatorTest {

    private Authenticator sut;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Mock
    private UserAccountDao userAccountDao;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Before
    public void injectMock() {
        sut = new Authenticator(userAccountDao, passwordEncoder);
    }

    @Test
    public void authenticate_KO_login_is_null() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Login can't be null.");
        sut.authenticate(null, "whatever");
    }

    @Test
    public void authenticate_KO_password_is_null() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Password can't be null.");
        sut.authenticate("whatever", null);
    }

    @Test
    public void authenticate_false_noPersistentUser() {
        final String login = "jdoe";
        given(userAccountDao.getByLogin(login)).willReturn(null);
        assertFalse(sut.authenticate(login, "whatever"));
        then(userAccountDao).should(only()).getByLogin(same(login));
        then(userAccountDao).should(never()).save(any(UserAccount.class));
    }

    @Test
    public void authenticate_false_wrongPassword() {
        final String login = "jdoe";
        final String password = "jdoepw";
        given(userAccountDao.getByLogin(login)).willReturn(new UserAccount("jdoe","XXXXXX"));
        given(passwordEncoder.encode(password)).willReturn("YYYYYY");

        assertFalse(sut.authenticate(login, "whatever"));

        then(userAccountDao).should(only()).getByLogin(same(login));
        then(userAccountDao).should(never()).save(any(UserAccount.class));
    }

    @Test
    public void authenticate_true() {

        Date now = new Date();
        ArgumentCaptor<UserAccount> argumentCaptor = ArgumentCaptor.forClass(UserAccount.class);

        UserAccount persistentUserAccount = new UserAccount("jdoe","XXXXXX");
        Date persistentLastConnectionDate = Date.from(LocalDate.now().minusDays(1).atStartOfDay().toInstant(ZoneOffset.UTC));;
        persistentUserAccount.setLastConnectionDate(persistentLastConnectionDate);

        final String login = "jdoe";
        final String password = "jdoepw";
        given(userAccountDao.getByLogin(login)).willReturn(persistentUserAccount);
        given(passwordEncoder.encode(password)).willReturn("XXXXXX");

        assertTrue(sut.authenticate(login, password));

        then(userAccountDao).should(times(1)).getByLogin(same(login));
        then(userAccountDao).should(times(1)).save(argumentCaptor.capture());
        UserAccount userAccount = argumentCaptor.getValue();
        Date lastConnectionDate = userAccount.getLastConnectionDate();
        assertNotEquals(persistentLastConnectionDate, lastConnectionDate);
        assertTrue(now.before(lastConnectionDate));
    }

}
