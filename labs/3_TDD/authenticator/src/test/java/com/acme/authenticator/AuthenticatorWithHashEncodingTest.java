package com.acme.authenticator;

import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

/**
 * Created by rguitter on 08/11/2016.
 */
@ActiveProfiles("HASH")
@ContextConfiguration(classes = AbstractAuthenticatorWithPwEncodingTest.TestConfig.class)
public class AuthenticatorWithHashEncodingTest extends AbstractAuthenticatorWithPwEncodingTest {
    public AuthenticatorWithHashEncodingTest() {
        super(String.valueOf("jdoepw".hashCode()));
    }
}
