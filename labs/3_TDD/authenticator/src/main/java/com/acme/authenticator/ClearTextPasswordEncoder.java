package com.acme.authenticator;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

/**
 * Created by rguitter on 07/11/2016.
 */
@Component
@Profile("CLEAR")
public class ClearTextPasswordEncoder implements PasswordEncoder {
    public String encode(String password) {
        if (password == null)
            throw new IllegalArgumentException("Can't encode a null password.");
        return password;
    }
}
