package com.acme.authenticator;

import java.util.Date;

/**
 * Created by rguitter on 07/11/2016.
 */
public class UserAccount {

    private String login;
    private String encodedPassword;
    private Date lastConnectionDate;

    public UserAccount(String login, String encodedPassword) {
        this.login = login;
        this.encodedPassword = encodedPassword;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEncodedPassword() {
        return encodedPassword;
    }

    public void setEncodedPassword(String encodedPassword) {
        this.encodedPassword = encodedPassword;
    }

    public Date getLastConnectionDate() {
        return lastConnectionDate;
    }

    public void setLastConnectionDate(Date lastConnectionDate) {
        this.lastConnectionDate = lastConnectionDate;
    }
}
