package com.acme.authenticator;

/**
 * Created by rguitter on 07/11/2016.
 */
public interface PasswordEncoder {
    String encode(String password);
}
