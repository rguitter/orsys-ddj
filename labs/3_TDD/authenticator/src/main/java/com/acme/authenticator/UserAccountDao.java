package com.acme.authenticator;

/**
 * Created by rguitter on 07/11/2016.
 */
public interface UserAccountDao {
    UserAccount getByLogin(String login);
    void save(UserAccount userAccount);
}
