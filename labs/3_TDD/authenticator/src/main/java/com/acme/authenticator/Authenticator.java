package com.acme.authenticator;

import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Created by rguitter on 07/11/2016.
 */
@Service
public class Authenticator {

    private final UserAccountDao userAccountDao;
    private final PasswordEncoder passwordEncoder;

    public Authenticator(UserAccountDao userAccountDao, PasswordEncoder passwordEncoder) {
        this.userAccountDao = userAccountDao;
        this.passwordEncoder = passwordEncoder;
    }

    public boolean authenticate(String login, String password) {

        if (login == null)
            throw new IllegalArgumentException("Login can't be null.");

        if (password == null)
            throw new IllegalArgumentException("Password can't be null.");

        UserAccount persistentUserAccount = userAccountDao.getByLogin(login);
        if (persistentUserAccount == null
                || !persistentUserAccount.getEncodedPassword().equals(passwordEncoder.encode(password)))
            return false;

        persistentUserAccount.setLastConnectionDate(new Date());
        userAccountDao.save(persistentUserAccount);
        return true;
    }
}
