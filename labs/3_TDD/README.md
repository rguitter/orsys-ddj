# Use case

An authenticator system with the following characteristics:

* A **User** is identified by its login and autthenticated by its password.
* On a successful authentication, we have to update the last connection date.
* A password may be stored encrypted. To keep it simple consider two possible encryption:
    - Clear text encryption (meaning no encryption in fact)
    - Hash based encryption (rely on String.hash() from java)
* **User** are stored in a DB.

# TDD

Write a kind of todo list about the scenario you want to implements

Follow the steps as in the slides:

* Elaborate one test
* Make it work by writing just enougth code to make it compile
* Run it and see it failing
* Make it pass
* Refactor code
* Repeat first steps

If in the middle of the process some new idea/case popped up, add it to your todo list to not ofrget it but complete your current task (don't switch).


# Tips

* For capturing the arguments of a call to a mock's method, you can use [ArgumentCaptor](http://site.mockito.org/mockito/docs/current/org/mockito/ArgumentCaptor.html).
* To assert on Rest Response body you can use [JsonPath from Jayway](https://github.com/jayway/JsonPath);