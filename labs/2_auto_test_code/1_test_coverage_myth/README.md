# Objectives

Check how easy it is to fool the usual coverage tool.

# Let's move on

Follow the steps of the following [example](https://github.com/rguitter/sb-java-testcoverage)

We will endup with a good inderstanding of coverage and mutant testing.
Based on this it would be nice to install a plugin for PITest, so:
* for eclipse, there's [pitclipse](http://marketplace.eclipse.org/content/pitclipse)
* for intellij, there's a [plugin](https://plugins.jetbrains.com/plugin/?idea&pluginId=7119)

Install the plugin and give it a try. 