# Introduction

The following [presentation](http://martinfowler.com/articles/microservice-testing/) from Martin Fowler propose some stategies to test a micro-services solution.
From this we will keep the separation of concerns by breaking our application in two main layer:

* Domain (the business logic)
* Protocol (the exposure logic, REST in that case)

Domain itself contains the service layer and an integration layer to some persistence.
Protocol is just the REST endpoint to expose the domain service.

# First Steps - Maven setup

Create a project with:

* groupId = com.acme
* artifactId = contact

To the `pom.xml` add the following:

```xml
     <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>io.spring.platform</groupId>
                <artifactId>platform-bom</artifactId>
                <version>Athens-RELEASE</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>

       <!-- Main dependencies -->
       <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-classic</artifactId>
            <version>1.1.7</version>
        </dependency>
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>javax.servlet-api</artifactId>
            <version>3.1.0</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-webmvc</artifactId>
            <exclusions>
                <exclusion>
                    <groupId>commons-logging</groupId>
                    <artifactId>commons-logging</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-tx</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-jdbc</artifactId>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>jcl-over-slf4j</artifactId>
            <version>1.7.21</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
            <version>2.8.4</version>
        </dependency>

        <!-- ## Test dependencies -->
        <!-- To be added -->

    </dependencies>
```

# The domain

Add to **src/main/java** the following packages 

* `com.acme.contact.domain.integration`
* `com.acme.contact.domain.service`

Create the same in **src/test/java**

## Domain service

### Implementation

* To `com.acme.contact.domain.service` add the following class

```java
public class Contact implements Serializable {

    private Long id;

    private String firstname;
    private String lastname;
    private String alias;
    private String emailAddress;

    public Contact() {
    }

    public Contact(String firstname, String lastname, String alias, String emailAddress) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.alias = alias;
        this.emailAddress = emailAddress;
    }

    // TODO generate the getter/setter
    
    // TODO generate toString() with the wizard of your IDE

}
```

* To `com.acme.contact.domain.integration` add the following interface

```java
public interface ContactDAO {

    Contact getByAlias(String alias);

    Long insert(Contact same);
}
```

* Back to `com.acme.contact.domain.service` add the following classes

```java
public class DuplicateAliasException extends Exception {

    public DuplicateAliasException(String alias, Contact persistentContact) {
        super("Alias '" + alias + "' is already used by " + persistentContact);
    }
}
```

```java
@Service
public class ContactService {

    private final ContactDAO dao;

    @Autowired
    public ContactService(ContactDAO dao) {
        this.dao = dao;
    }

    public void save(Contact transientContact) throws DuplicateAliasException {
        Contact persistentContact = dao.getByAlias(transientContact.getAlias());
        if (persistentContact != null)
            throw new DuplicateAliasException(transientContact.getAlias(), persistentContact);
        final Long id = dao.insert(transientContact);
        transientContact.setId(id);
    }
}
```
### Testing

#### Solitary Unit Testing

Start by maven: go to the `pom.xml` to add **junit** and **mockito-all** (last version for each).
I you have any problem to figure out what to add esxactly check on [mvnrepository](https://mvnrepository.com/).

In **src/test/java** add the following test class:

```java
@RunWith(MockitoJUnitRunner.class)
public class ContactServiceTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @InjectMocks
    private ContactService sut;

    @Mock
    private ContactDAO contactDAO;

    @Test
    public void createContact_ko_duplicateAlias() throws Exception {

        // Given
        Contact persistentContact = new Contact("Jimmy", "Doe", "jdoe", "jimmy.doe@gmail.com");
        Contact transientContact = new Contact("John","Doe", "jdoe", "jdoe@gmail.com");
        when(contactDAO.getByAlias("jdoe")).thenReturn(persistentContact);
        expectedException.expect(DuplicateAliasException.class);
        expectedException.expectMessage("Alias 'jdoe' is already used by " + persistentContact);
        
        //When
        sut.save(transientContact);
        
        // Then
        assertNull(transientContact.getId());
        verify(contactDAO.insert(any(Contact.class)), never());
    }

    @Test
    public void createContact_ok() throws Exception {
        
        // Given
        Contact transientContact = new Contact("John","Doe", "jdoe", "jdoe@gmail.com");
        when(contactDAO.getByAlias("jdoe")).thenReturn(null);
        when(contactDAO.insert(same(transientContact))).thenReturn(101L);
        
        // When
        sut.save(transientContact);
        
        // Then
        assertTrue(101L == transientContact.getId());
    }

}
``` 

Add static import like this `import static org.junit.Assert.assertNull;` to make it compile then run it to see it succeed.

As you see all the magic happens thanks to:

* The annotations
    - `@RunWith`, `@Rule` and `@Test` from junit
    - `@InjectMocks` and `@Mock` from Mockito
* The `MockitoJUnitRunner` class which will take care of all the annotations
    - Create the class to test and inject the mock
* Mockito api
    - when(...).thenReturn(...) which help define stubs on the mock objects.

Further readings:

* [Junit](http://junit.org/junit4/)
* In particular from the previous example, you give a look to [Exception testing](https://github.com/junit-team/junit4/wiki/Exception-testing)
* [Mockito - docs](http://site.mockito.org/mockito/docs/current/org/mockito/Mockito.html)

### Move forward

Add the features **Get All Contact**, **Update Contact** and **Delete Contact** and write the relevant test.
Try to stick with commonly used pattern: Given, When, Then.
Don't hesitate to run PITest to check your coverage.

## Domain integration

### Implementation

In **src/main/java** add the following add the following class to the package `com.acme.contact.domain.integration`

```java
@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class JdbcContactDAO implements ContactDAO {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public JdbcContactDAO(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public Contact getByAlias(String alias) {
        return jdbcTemplate.queryForObject("select * from T_CONTACT where ALIAS = ?", new Object[]{alias}, new RowMapper<Contact>() {
            public Contact mapRow(ResultSet rs, int rowNum) throws SQLException {
                return new Contact(rs.getString("FIRSTNAME"), rs.getString("LASTNAME"), rs.getString("ALIAS"), rs.getString("EMAIL_ADDRESS"));
            }
        });
    }

    public Long insert(final Contact contact) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement pStmt = connection.prepareStatement("insert into T_CONTACT (FIRSTNAME, LASTNAME, ALIAS, EMAIL_ADDRESS) values (?,?,?,?)");
                pStmt.setString(1, contact.getFirstname());
                pStmt.setString(2, contact.getLastname());
                pStmt.setString(3, contact.getAlias());
                pStmt.setString(4, contact.getEmailAddress());
                return pStmt;
            }
        }, keyHolder);
        return keyHolder.getKey().longValue();
    }

}
```

As you can see we will use spring to power our app.

### Testing

#### Step 0

In **src/test/resources** add the following 

* `logback.xml` config file (this is not mandatory but it will helps to have usable logs)

```xml
<configuration>

    <appender name="STDOUT" class="ch.qos.logback.core.ConsoleAppender">
        <!-- encoders are assigned the type
             ch.qos.logback.classic.encoder.PatternLayoutEncoder by default -->
        <encoder>
            <pattern>%d{HH:mm:ss.SSS} [%thread] %-5level %logger{36} - %msg%n</pattern>
        </encoder>
    </appender>

    <root level="info">
        <appender-ref ref="STDOUT" />
    </root>
</configuration>
```

* `schema.sql.ddl`

```sql
CREATE TABLE T_CONTACT (
  ID            IDENTITY PRIMARY KEY,
  FIRSTNAME     CHAR(50),
  LASTNAME      VARCHAR(50),
  ALIAS         VARCHAR(25),
  EMAIL_ADDRESS VARCHAR(255)
);
```

In **src/test/java**, create the following class in package `com.acme.contact.domain.integration``

```java
@RunWith(SpringRunner.class)
@Transactional
@Rollback
public class JdbcContactDAOTest {

    // TODO Step 1 test config (DB and object creation)

    @Autowired
    private ContactDAO sut;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    DataSource dataSource;

    @Before
    public void prepare() throws Exception {
        // TODO step 2 prepare a data set
    }

    @Test
    public void insert() {
        Contact transientContact = new Contact("Dom", "Doe", "ddoe", "ddoe@gmail.com");
        int count = JdbcTestUtils.countRowsInTable(jdbcTemplate, "T_CONTACT");
        Long id = sut.insert(transientContact);
        assertNotNull(id);
        assertEquals(count + 1, JdbcTestUtils.countRowsInTable(jdbcTemplate, "T_CONTACT"));

    }

    @Test
    public void getByAlias() {
        Contact contact = sut.getByAlias("jdoe");
        assertNotNull(contact);
        assertEquals("John", contact.getFirstname());
        assertEquals("Doe", contact.getLastname());
        assertEquals("jdoe", contact.getAlias());
        assertEquals("jdoe@gmail.com", contact.getEmailAddress());
    }


    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    @Transactional(propagation = Propagation.NEVER)
    public void failIfNoTransaction() {
        expectedException.expect(IllegalTransactionStateException.class);
        expectedException.expectMessage("No existing transaction found for transaction marked with propagation 'mandatory'");
        sut.getByAlias("jdoe");
    }
}    
``` 

In order to make this compile we have to add the following dependency to the `pom.xml``

```xml
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
            <exclusions>
                <exclusion>
                    <groupId>commons-logging</groupId>
                    <artifactId>commons-logging</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
```

As you can see from the first annotations the test will run in transaction, meaning that 
before a each test a transaction is open, after each test it is rollback leaving a dataset completly clean, 
while the `jdbcTemplate` keeps on being able to perform DB assertion since in run in the same transaction than our code.


The last test is here to prove than the annotation `@Transactional(propagation=MANDATORY)` is present in the DAO implementation.
This is to ensure that a DAO is always call within a transaction but it's not up to the DAO to create one 
(the transaction boundary remains to be defined: at domain service level or at protocol request level).

### Step 1 - boostrap the project

Everything will happen within Spring => we need to configure a Spring context.
We will have a code centric approach by defining the spring configuration as a static class within our test class.
Then to understand the test we only have to open our test class (no more need to jump to whatever xml config files)

Documentation highlight:

* [Spring java config](http://docs.spring.io/spring/docs/current/spring-framework-reference/html/beans.html#beans-java)
* [Spring unit testing](http://docs.spring.io/spring/docs/current/spring-framework-reference/html/integration-testing.html)
* In particular [Spring embedded database boostrap](http://docs.spring.io/spring/docs/current/spring-framework-reference/html/jdbc.html#jdbc-embedded-database-support)

So int the test class add the following snippet as //TODO Step 1

```java
    @Configuration
    @ComponentScan
    @EnableTransactionManagement
    public static class TestConfig {

        @Bean
        public DataSource dataSource() {
            return new EmbeddedDatabaseBuilder()
                    .setType(EmbeddedDatabaseType.H2)
                    .addScript("schema.sql.ddl")
                    .build();
        }

        @Bean
        public PlatformTransactionManager transactionManager() {
            return new DataSourceTransactionManager(dataSource());
        }

        @Bean
        public JdbcTemplate jdbcTemplate() {
            return new JdbcTemplate(dataSource());
        }

    }
```

If you run it like this it will complain that H2 drivier is not in the classpath => we need to add a dependency in the `pom.xml``

### Step 2 - data set definition

There's several approach for this. We will keep the code centric one by using a very simple library with no transitive dependency :) : [DBSetup](http://dbsetup.ninja-squad.com/)

Todo so copy the following snippet to the method `prepare()`:

```java
        Operation operation =
                sequenceOf(
                        deleteAllFrom("T_CONTACT"),
                        insertInto("T_CONTACT")
                                .columns("FIRSTNAME", "LASTNAME", "ALIAS", "EMAIL_ADDRESS")
                                .values("John", "Doe", "jdoe", "jdoe@gmail.com")
                                .values("Fred", "Doe", "fdoe", "fdoe@gmail.com")
                                .build());
        DbSetup dbSetup = new DbSetup(new DataSourceDestination(dataSource), operation);
        dbSetup.launch();
```

Now you can run the test to see it succeed.

### Step 2 - refinement (optional)

Since the test run in transaction, it's a pity to let the dataset being re-initialized for each test.
DBSetup support a nice feature to mark a dataset fixture as non dirty => no need to reset it.

To do so you can to the test class `private static DbSetupTracker dbSetupTracker = new DbSetupTracker();``
which track the need to reset or not the data. then in the method `prepare()` you replace the last imperative line 
`dbSetup.launch();` by `dbSetupTracker.launchIfNecessary(dbSetup);`. Finally by adding this snippet you tell 
to DBSetup to never reset data (which is ok since we run test in transaction):

```java
    @After
    public void skipNextLaunch() {
        dbSetupTracker.skipNextLaunch();
    }
```

### Move forward

Implement the logic that you add to the service (Get all, updat contact, delete contact) and test it.

# The protocol

## Implementation

Here we will consider an exposure of the domain through REST.
There's two main ways to implement this in java:

* Spring MVC
* JAX-WS

We will user spring MVC.

Let's add the following class to **src/main/java** in the package `com.acme.contact.protocol``

```java
@RestController
@RequestMapping("/contacts")
public class ContactServiceEndpoint {

    private static final Logger logger = LoggerFactory.getLogger(ContactServiceEndpoint.class);

    @Autowired
    private ContactService contactService;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity create(UriComponentsBuilder ucb, @RequestBody Contact contact) {

        try {
            contactService.save(contact);
            return ResponseEntity.created(ucb.path("/contacts/{id}").buildAndExpand(contact.getId()).toUri()).build();
        } catch (DuplicateAliasException e) {
            logger.error(e.getMessage(), e);
            return ResponseEntity.status(INTERNAL_SERVER_ERROR).contentType(MediaType.TEXT_PLAIN).body(e.getMessage());
        }

    }
}
```

From that implementation, you see that we should test:

* the call to the service itself
* the logging which occures on the domain DuplicateAliasException
* the security check

Good news, all this can be done as Sociable Unit Testing, thanks to [Spring Mock MVC](http://docs.spring.io/spring/docs/current/spring-framework-reference/html/integration-testing.html#spring-mvc-test-framework) .
This [article](https://blog.zenika.com/2013/01/15/rest-web-services-testing-with-spring-mvc/) from Zenika can help in order to complete your understanding.

## Testing REST endpoint

Let's add the following class to **src/test/java** in package `com.acme.contact.protocol``

```java
@RunWith(SpringRunner.class)
@WebAppConfiguration
public class ContactServiceEndpointTest {

    @Configuration
    @EnableWebMvc
    @ComponentScan(basePackageClasses = {ContactServiceEndpoint.class, ContactService.class})
    public static class TestConfig {
        @Bean
        public ContactDAO contactDAO() {
            return Mockito.mock(ContactDAO.class);
        }
    }

    @Autowired
    ContactDAO contactDAO;

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    public void post_OK() throws Exception {
        String contactAsJson = new ObjectMapper().writeValueAsString(new Contact("John", "Doe", "jdoe", "jdoe@gmail.com"));
        when(contactDAO.getByAlias("jdoe")).thenReturn(null);
        when(contactDAO.insert(any(Contact.class))).thenReturn(1L);
        this.mockMvc.perform(
                post("/contacts")
                        .content(contactAsJson)
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(redirectedUrl("http://localhost/contacts/1"))
                .andExpect(header().string("Location", "http://localhost/contacts/1"));
    }

    @Test
    public void post_KO() throws Exception {
        String contactAsJson = new ObjectMapper().writeValueAsString(new Contact("John", "Doe", "jdoe", "jdoe@gmail.com"));
        when(contactDAO.getByAlias("jdoe")).thenReturn(new Contact("Jimmy", "Doe", "jdoe", "jimmy.doe@gmail.com"));
        this.mockMvc.perform(
                post("/contacts")
                        .content(contactAsJson)
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andDo(print())
                .andExpect(status().is5xxServerError())
                .andExpect(content().contentType(MediaType.TEXT_PLAIN_VALUE))
                .andExpect(content().string(CoreMatchers.startsWith("Alias 'jdoe' is already used by ")));
        //.andExpect(jsonPath("$.name").value("Lee"));
    }

}
```

As you can we re-use spring java config. Here we load all our classes except the DAO to avoid the need of setting up a DB.
That's why those tests are sociable unit testing (pure java, no DB, nothing else).
The mock mvc related stuff comes from the documentation (mention above).

Move forward and implement the endpoints of the feature you add to the domain (get all, ...).
Write the test of course :)

## Testing the logging strategy

Give a look to this [post](http://stackoverflow.com/questions/29076981/how-to-intercept-slf4j-logging-via-a-junit-test)
It should be nice to apply this to check that DuplicateAliasException is correctly logged.
Logging is always underestimate because developpers seem to forget that they will probably maintain it in production.
That's one difference I do between a project and a product: product goes in production and fails sometimes.

## Testing the security

To secure such application we will typically use [Spring Security](http://docs.spring.io/spring-security/site/docs/current/reference/html/index.html).
That framework come with a nice tool set for testing as you can see from [Documentation - mockmvc+security](http://docs.spring.io/spring-security/site/docs/current/reference/html/test-mockmvc.html).

### Maven setup

Add to the `pom.xml` the following dependencies:

```xml
        <dependency>
            <groupId>org.springframework.security</groupId>
            <artifactId>spring-security-web</artifactId>
        </dependency>
```
```xml
        <dependency>
            <groupId>org.springframework.security</groupId>
            <artifactId>spring-security-config</artifactId>
        </dependency>
```
```xml
       <dependency>
            <groupId>org.springframework.security</groupId>
            <artifactId>spring-security-test</artifactId>
        </dependency>
```

### Configuration

Add to the package `om.acme.contact.protocal` in **src/main/java** the following configuration class:
 
```java
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/contacts").hasRole("CONTACT")
                //.and()
                //.formLogin().loginPage("/login").failureUrl("/login-error")
                ;
    }

}
```

### Test the security

Let's create a dedicated test class next to `ContactServiceEndpoint` as follow:

```java
@RunWith(SpringRunner.class)
@WebAppConfiguration
public class ContactServiceEnpoitWithSecurityTest {

    @Configuration
    @EnableWebMvc
    @ComponentScan(basePackageClasses = {ContactServiceEndpoint.class, ContactService.class})
    @Import(SecurityConfig.class)
    public static class TestConfig {
    }

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).apply(springSecurity()).build();
    }

    @Test
    public void post_KO_Unauthorized() throws Exception {
        testPost(HttpStatus.FORBIDDEN);
    }

    @Test
    @WithMockUser
    public void post_KO_Forbidden() throws Exception {
        testPost(HttpStatus.FORBIDDEN);
    }

    @Test
    @WithMockUser(roles = {"CONTACT"})
    public void post_OK() throws Exception {
        testPost(HttpStatus.CREATED);
    }

    private void testPost(HttpStatus expectedStatus) throws Exception {
        String contactAsJson = new ObjectMapper().writeValueAsString(new Contact("John", "Doe", "jdoe", "jdoe@gmail.com"));
        this.mockMvc.perform(
                post("/contacts")
                        .with(csrf())
                        .content(contactAsJson)
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andDo(print())
                .andExpect(status().is(expectedStatus.value()));
    }

}
```

Compare to the other test class, there's 

* two changes in the setup:
  - **`@Import(SecurityConfig.class)`** has been added to our test configuration class to activate our security configuration (this equivalent to the **`<import/>`** directive in the spring xml configuration)
  - **`apply(springSecurity())`** has been added within the method `@Before public void setup()`
* two change in the test methods:
  - **`.with(csrf())`** has been added to the method `perform(post(...))` because csrf protection is active by default in Spring Security since version 4.
  - **`@WithMockUser`** has used to define which user run the test (there's a programactic way to achieve the same by statically importing `org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*` as mention in the documentation) 
 
### Move forward  
  
Uncomment the following lines from `SecurityConfig`:

```java
                //.and()
                //.formLogin().loginPage("/login").failureUrl("/login-error")
```

Then run the test to see what happens.