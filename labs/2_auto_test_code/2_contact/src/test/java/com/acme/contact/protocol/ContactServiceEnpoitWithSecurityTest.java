package com.acme.contact.protocol;

import com.acme.contact.domain.service.Contact;
import com.acme.contact.domain.service.ContactService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by rguitter on 01/11/2016.
 */
@RunWith(SpringRunner.class)
@WebAppConfiguration
public class ContactServiceEnpoitWithSecurityTest {

    @Configuration
    @EnableWebMvc
    @ComponentScan(basePackageClasses = {ContactServiceEndpoint.class, ContactService.class})
    @Import(SecurityConfig.class)
    public static class TestConfig {
    }

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).apply(springSecurity()).build();
    }

    @Test
    public void post_KO_Unauthorized() throws Exception {
        testPost(HttpStatus.FORBIDDEN);
    }

    @Test
    @WithMockUser
    public void post_KO_Forbidden() throws Exception {
        testPost(HttpStatus.FORBIDDEN);
    }

    @Test
    @WithMockUser(roles = {"CONTACT"})
    public void post_OK() throws Exception {
        testPost(HttpStatus.CREATED);
    }

    private void testPost(HttpStatus expectedStatus) throws Exception {
        String contactAsJson = new ObjectMapper().writeValueAsString(new Contact("John", "Doe", "jdoe", "jdoe@gmail.com"));
        this.mockMvc.perform(
                post("/contacts")
                        .with(csrf())
                        .content(contactAsJson)
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andDo(print())
                .andExpect(status().is(expectedStatus.value()));
    }

}
