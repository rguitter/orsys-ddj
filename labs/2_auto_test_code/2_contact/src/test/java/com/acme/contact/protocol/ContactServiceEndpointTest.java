package com.acme.contact.protocol;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import com.acme.contact.domain.integration.ContactDAO;
import com.acme.contact.domain.service.Contact;
import com.acme.contact.domain.service.ContactService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by rguitter on 27/10/2016.
 */
@RunWith(SpringRunner.class)
@WebAppConfiguration
public class ContactServiceEndpointTest {

    @Configuration
    @EnableWebMvc
    @ComponentScan(basePackageClasses = {ContactServiceEndpoint.class, ContactService.class})
    public static class TestConfig {
        @Bean
        public ContactDAO contactDAO() {
            return Mockito.mock(ContactDAO.class);
        }
    }

    @Autowired
    ContactDAO contactDAO;

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }


    @Test
    public void post_OK() throws Exception {

        // Given
        String contactAsJson = new ObjectMapper().writeValueAsString(new Contact("John", "Doe", "jdoe", "jdoe@gmail.com"));
        when(contactDAO.getByAlias("jdoe")).thenReturn(null);
        when(contactDAO.insert(any(Contact.class))).thenReturn(1L);

        // When
        this.mockMvc.perform(
                post("/contacts")
                        .content(contactAsJson)
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andDo(print())

                // Then
                .andExpect(status().isCreated())
                .andExpect(redirectedUrl("http://localhost/contacts/1"))
                .andExpect(header().string("Location", "http://localhost/contacts/1"));
    }

    @Test
    public void post_KO() throws Exception {

        // Given
        String contactAsJson = new ObjectMapper().writeValueAsString(new Contact("John", "Doe", "jdoe", "jdoe@gmail.com"));
        when(contactDAO.getByAlias("jdoe")).thenReturn(new Contact("Jimmy", "Doe", "jdoe", "jimmy.doe@gmail.com"));

        // When
        this.mockMvc.perform(
                post("/contacts")
                        .content(contactAsJson)
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andDo(print())

                // Then
                .andExpect(status().is5xxServerError())
                .andExpect(content().contentType(MediaType.TEXT_PLAIN_VALUE))
                .andExpect(content().string("Alias 'jdoe' is already used by Contact{id=null, firstname='Jimmy', lastname='Doe', alias='jdoe', emailAddress='jimmy.doe@gmail.com'}"));
        //.andExpect(jsonPath("$.name").value("Lee"));
    }

    @Test
    public void post_KO_check_logs() throws Exception {

        // Given
        TestAppender.reset();
        String contactAsJson = new ObjectMapper().writeValueAsString(new Contact("John", "Doe", "jdoe", "jdoe@gmail.com"));
        when(contactDAO.getByAlias("jdoe")).thenReturn(new Contact("Jimmy", "Doe", "jdoe", "jimmy.doe@gmail.com"));

        // When
        this.mockMvc.perform(
                post("/contacts")
                        .content(contactAsJson)
                        .contentType(MediaType.APPLICATION_JSON_UTF8));

        // Then
        assertEquals(1, TestAppender.events.size());
        ILoggingEvent event = TestAppender.events.get(0);
        assertEquals(Level.ERROR, event.getLevel());
        assertEquals("Alias 'jdoe' is already used by Contact{id=null, firstname='Jimmy', lastname='Doe', alias='jdoe', emailAddress='jimmy.doe@gmail.com'}", event.getMessage());
    }

}
