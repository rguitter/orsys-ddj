package com.acme.contact.protocol;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.AppenderBase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rguitter on 01/11/2016.
 */
public class TestAppender extends AppenderBase<ILoggingEvent> {

    public static List<ILoggingEvent> events = new ArrayList<>();

    public static void reset() {
        events.clear();
    }

    @Override
    protected void append(ILoggingEvent e) {
        events.add(e);
    }

}
