package com.acme.contact.domain.integration;

import com.acme.contact.domain.service.Contact;
import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.DbSetupTracker;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.jdbc.JdbcTestUtils;
import org.springframework.transaction.IllegalTransactionStateException;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;

import static com.ninja_squad.dbsetup.Operations.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by rguitter on 28/10/2016.
 */
@RunWith(SpringRunner.class)
@Transactional
@Rollback
public class JdbcContactDAOTest {

    @Configuration
    @ComponentScan
    @EnableTransactionManagement
    public static class TestConfig {

        @Bean
        public DataSource dataSource() {
            return new EmbeddedDatabaseBuilder()
                    .setType(EmbeddedDatabaseType.H2)
                    .addScript("schema.sql.ddl")
                    .build();
        }

        @Bean
        public PlatformTransactionManager transactionManager() {
            return new DataSourceTransactionManager(dataSource());
        }

        @Bean
        public JdbcTemplate jdbcTemplate() {
            return new JdbcTemplate(dataSource());
        }

    }

    @Autowired
    private ContactDAO sut;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    DataSource dataSource;

    private static DbSetupTracker dbSetupTracker = new DbSetupTracker();

    @Before
    public void prepare() throws Exception {
        Operation operation =
                sequenceOf(
                        deleteAllFrom("T_CONTACT"),
                        insertInto("T_CONTACT")
                                .columns("FIRSTNAME", "LASTNAME", "ALIAS", "EMAIL_ADDRESS")
                                .values("John", "Doe", "jdoe", "jdoe@gmail.com")
                                .values("Fred", "Doe", "fdoe", "fdoe@gmail.com")
                                .build());
        DbSetup dbSetup = new DbSetup(new DataSourceDestination(dataSource), operation);
        //dbSetup.launch();
        dbSetupTracker.launchIfNecessary(dbSetup);
    }

    @After
    public void skipNextLaunch() {
        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void insert() {

        // Given
        Contact transientContact = new Contact("Dom", "Doe", "ddoe", "ddoe@gmail.com");
        int count = JdbcTestUtils.countRowsInTable(jdbcTemplate, "T_CONTACT");

        // When
        Long id = sut.insert(transientContact);

        // Then
        assertNotNull(id);
        assertEquals(count + 1, JdbcTestUtils.countRowsInTable(jdbcTemplate, "T_CONTACT"));

    }

    @Test
    public void getByAlias() {

        // When
        Contact contact = sut.getByAlias("jdoe");

        // Then
        assertNotNull(contact);
        assertEquals("John", contact.getFirstname());
        assertEquals("Doe", contact.getLastname());
        assertEquals("jdoe", contact.getAlias());
        assertEquals("jdoe@gmail.com", contact.getEmailAddress());
    }


    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    @Transactional(propagation = Propagation.NEVER)
    public void failIfNoTransaction() {

        // Given
        expectedException.expect(IllegalTransactionStateException.class);
        expectedException.expectMessage("No existing transaction found for transaction marked with propagation 'mandatory'");

        // When
        sut.getByAlias("jdoe");
    }

}
