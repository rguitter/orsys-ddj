package com.acme.contact.domain.service;

import com.acme.contact.domain.integration.ContactDAO;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

/**
 * Created by rguitter on 27/10/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class ContactServiceTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @InjectMocks
    private ContactService sut;

    @Mock
    private ContactDAO contactDAO;

    @Test
    public void createContact_ko_duplicateAlias() throws Exception {

        // Given
        Contact persistentContact = new Contact("Jimmy", "Doe", "jdoe", "jimmy.doe@gmail.com");
        Contact transientContact = new Contact("John","Doe", "jdoe", "jdoe@gmail.com");
        when(contactDAO.getByAlias("jdoe")).thenReturn(persistentContact);
        expectedException.expect(DuplicateAliasException.class);
        expectedException.expectMessage("Alias 'jdoe' is already used by " + persistentContact);

        // When
        sut.save(transientContact);

        // Then
        assertNull(transientContact.getId());
        verify(contactDAO.insert(any(Contact.class)), never());
    }

    @Test
    public void createContact_ok() throws Exception {

        // Given
        Contact transientContact = new Contact("John","Doe", "jdoe", "jdoe@gmail.com");
        when(contactDAO.getByAlias("jdoe")).thenReturn(null);
        when(contactDAO.insert(same(transientContact))).thenReturn(101L);

        // When
        sut.save(transientContact);

        // Then
        assertTrue(101L == transientContact.getId());
    }

}
