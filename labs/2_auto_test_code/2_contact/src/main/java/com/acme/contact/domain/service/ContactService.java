package com.acme.contact.domain.service;

import com.acme.contact.domain.integration.ContactDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by rguitter on 27/10/2016.
 */
@Service
public class ContactService {

    private final ContactDAO dao;

    @Autowired
    public ContactService(ContactDAO dao) {
        this.dao = dao;
    }

    public void save(Contact transientContact) throws DuplicateAliasException {
        Contact persistentContact = dao.getByAlias(transientContact.getAlias());
        if (persistentContact != null)
            throw new DuplicateAliasException(transientContact.getAlias(), persistentContact);
        final Long id = dao.insert(transientContact);
        transientContact.setId(id);
    }
}
