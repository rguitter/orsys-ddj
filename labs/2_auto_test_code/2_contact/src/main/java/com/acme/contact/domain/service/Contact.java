package com.acme.contact.domain.service;

import java.io.Serializable;

/**
 * Created by rguitter on 27/10/2016.
 */
public class Contact implements Serializable {

    private Long id;

    private String firstname;
    private String lastname;
    private String alias;
    private String emailAddress;

    public Contact() {
    }

    public Contact(String firstname, String lastname, String alias, String emailAddress) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.alias = alias;
        this.emailAddress = emailAddress;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "id=" + id +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", alias='" + alias + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                '}';
    }
}
