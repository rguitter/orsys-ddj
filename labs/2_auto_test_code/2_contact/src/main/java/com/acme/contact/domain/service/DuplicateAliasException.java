package com.acme.contact.domain.service;

/**
 * Created by rguitter on 27/10/2016.
 */
public class DuplicateAliasException extends Exception {

    public DuplicateAliasException(String alias, Contact persistentContact) {
        super("Alias '"+ alias + "' is already used by " + persistentContact);
    }
}
