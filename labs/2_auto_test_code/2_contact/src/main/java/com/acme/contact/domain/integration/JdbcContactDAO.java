package com.acme.contact.domain.integration;

import com.acme.contact.domain.service.Contact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by rguitter on 28/10/2016.
 */
@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class JdbcContactDAO implements ContactDAO {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public JdbcContactDAO(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public Contact getByAlias(String alias) {
        return jdbcTemplate.queryForObject("select * from T_CONTACT where ALIAS = ?", new Object[]{alias}, new RowMapper<Contact>() {
            public Contact mapRow(ResultSet rs, int rowNum) throws SQLException {
                return new Contact(rs.getString("FIRSTNAME"), rs.getString("LASTNAME"), rs.getString("ALIAS"), rs.getString("EMAIL_ADDRESS"));
            }
        });
    }

    public Long insert(final Contact contact) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement pStmt = connection.prepareStatement("insert into T_CONTACT (FIRSTNAME, LASTNAME, ALIAS, EMAIL_ADDRESS) values (?,?,?,?)");
                pStmt.setString(1, contact.getFirstname());
                pStmt.setString(2, contact.getLastname());
                pStmt.setString(3, contact.getAlias());
                pStmt.setString(4, contact.getEmailAddress());
                return pStmt;
            }
        }, keyHolder);
        return keyHolder.getKey().longValue();
    }

}
