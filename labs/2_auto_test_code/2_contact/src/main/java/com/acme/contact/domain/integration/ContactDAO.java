package com.acme.contact.domain.integration;

import com.acme.contact.domain.service.Contact;

/**
 * Created by rguitter on 27/10/2016.
 */
public interface ContactDAO {

    Contact getByAlias(String alias);

    Long insert(Contact same);
}
