package com.acme.contact.protocol;

import com.acme.contact.domain.service.Contact;
import com.acme.contact.domain.service.ContactService;
import com.acme.contact.domain.service.DuplicateAliasException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

/**
 * Created by rguitter on 27/10/2016.
 */
@RestController
@RequestMapping("/contacts")
public class ContactServiceEndpoint {

    private static final Logger logger = LoggerFactory.getLogger(ContactServiceEndpoint.class);

    @Autowired
    private ContactService contactService;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity create(UriComponentsBuilder ucb, @RequestBody Contact contact) {

        try {
            contactService.save(contact);
            return ResponseEntity.created(ucb.path("/contacts/{id}").buildAndExpand(contact.getId()).toUri()).build();
        } catch (DuplicateAliasException e) {
            logger.error(e.getMessage(), e);
            return ResponseEntity.status(INTERNAL_SERVER_ERROR).contentType(MediaType.TEXT_PLAIN).body(e.getMessage());
        }

    }
}
