# Objectives

We should have a complete tool box for code testing: unit (solitary, sociable) and integration (with BDD and Web Service).  
Once familiar with all those technics we can consider TDD from the next chapter.
**The material here is a must have for next chapter.**

To summarize, we should see:

* Junit as test engine
* Mockito for solitary testing (mocking, spying on collaborators)
* Spring for sociable and integration testing
* Relative to DB integration testing
    - H2 an embedded java DB
    - DBSetup from NinjaSquad for DB provisionning 
* WireMock (optional) for stubbing a web service 

# Agenda:

* We will start with test coverage to drive the test. So start by **1_test_coverage_myth**
* Then we will go for unit and integration testing with a small project **2_contact**.
* Optionally we can also give a try to [WireMock](http://wiremock.org/docs/) to complete our tool set when it comes to integration testing with Web Service in the picture.


