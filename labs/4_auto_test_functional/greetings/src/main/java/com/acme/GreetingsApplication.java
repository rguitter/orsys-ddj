package com.acme;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@SpringBootApplication
@RestController
public class GreetingsApplication {

	public static void main(String[] args) {
		SpringApplication.run(GreetingsApplication.class, args);
	}

	@GetMapping(value = "/greetings", produces = MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<String> sayHello(@RequestParam(required = false) String name) {
		return ResponseEntity.ok("Hello " + ((name != null) ? name : "World"));
	}

}
