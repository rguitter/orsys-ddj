package com.acme;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.equalTo;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GreetingsApplicationTests {

    @Test
    @Category(UnitTest.class)
    public void contextLoads() {
    }

    @Test
    @Category(IntegrationTest.class)
    public void restController_default() {
        when().get("/greetings").
        then().statusCode(200).body(equalTo("Hello World"));
    }

    @Test
    @Category(IntegrationTest.class)
    public void restController_with_name() {
        when().get("/greetings?name=Richard").
                then().statusCode(200).body(equalTo("Hello Richard"));
    }

}
