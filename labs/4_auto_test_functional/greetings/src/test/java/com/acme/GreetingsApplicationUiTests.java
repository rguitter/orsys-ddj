package com.acme;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.net.MalformedURLException;

import static org.junit.Assert.assertEquals;

/**
 * Created by rguitter on 02/11/2016.
 */
public class GreetingsApplicationUiTests {

    @BeforeClass
    public static void defineDrivers() {
        System.setProperty("webdriver.chrome.driver", "../chromedriver"); // may be chromediver.exe if on windows
    }

    @Test
    @Category(IntegrationTest.class)
    public void test_without_a_name() throws MalformedURLException {
        WebDriver driver = new ChromeDriver();
        try {
            GreetingsPage page = GreetingsPage.get(driver, "http://localhost:8080");
            page.sayHello(null);
            assertEquals("Hello World!", page.getHelloMessage());
        } finally {
            driver.quit();
        }
    }

    @Test
    @Category(IntegrationTest.class)
    public void test_with_a_name() throws MalformedURLException {
        WebDriver driver = new ChromeDriver();
        try {
            GreetingsPage page = GreetingsPage.get(driver, "http://localhost:8080");
            page.sayHello("Richard");
            assertEquals("Hello Richard!", page.getHelloMessage());
        } finally {
            driver.quit();
        }
    }

}
