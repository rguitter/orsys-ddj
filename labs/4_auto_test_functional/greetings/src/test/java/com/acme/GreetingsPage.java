package com.acme;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by rguitter on 02/11/2016.
 */
public class GreetingsPage {

    private final WebDriver driver;

    public static GreetingsPage get(WebDriver driver, String baseUrl) {
        driver.get(baseUrl);
        return new GreetingsPage(driver);
    }

    private GreetingsPage(WebDriver driver) {
        this.driver = driver;
        if (!"Greetings".equals(driver.getTitle()))
            throw new IllegalStateException("The current page is not 'Greetings' but " + driver.getCurrentUrl());
    }

    public void sayHello(String name) {
        if (name != null)
            driver.findElement(By.cssSelector("input[type=text]")).sendKeys(name);
        driver.findElement(By.cssSelector("input[type=button")).click();
    }

    public String getHelloMessage() {
        return driver.findElement(By.id("message")).getText();
    }

}
