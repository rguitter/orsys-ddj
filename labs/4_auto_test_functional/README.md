# Introduction

See how we can test a web ui or a REST service

# Use case - Hello World!

## Bootstrap

* Create a spring boot. To do so go to [initializr](http://start.spring.io/) and fill the form like this:
  - Group = com.acme
  - Artifact = greetings
  - Dependencies = Web
* Then generate the project. (You have an expanded version to see all the possible options)
* Unzip the archive and import it to your IDE.

## The REST service layer

### The rest controller

Open the class `GreetingsApplication` then 

* add `@RestController` to the class itself
* add the following method:

```java
    @GetMapping(value = "/greetings", produces = MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<String> sayHello(@RequestParam(required = false) String name) {
		return ResponseEntity.ok("Hello " + ((name != null) ? name : "World"));
	}
```

Run it and check in the browser that the service behaves correctly.

### Test the rest controller

We won't use mockmvc since we want to test the rest service while running (to avoid testing it within a browser).
We can use [Rest-assured](http://rest-assured.io/) for this.

First add the **pom.xml** the following dependency

```xml
		<dependency>
			<groupId>io.rest-assured</groupId>
			<artifactId>rest-assured</artifactId>
			<version>3.0.0</version>
			<scope>test</scope>
		</dependency>
```

Then add to the class `GreetingsApplicationTest` the following test methods:

```java
    @Test
    public void restController_default() {
        when().get("/greetings").
        then().statusCode(200).body(equalTo("Hello World"));
    }

    @Test
    public void restController_with_name() {
        when().get("/greetings?name=Richard").
                then().statusCode(200).body(equalTo("Hello Richard"));
    }
```

Run the test (be sure to have previously run the application otherwise you will a ConnectionRefuseException)

## The ui

### Implementation based on Angularjs

Add to the **pom.xml** the following dependency:

```xml
        <dependency>
			<groupId>org.webjars</groupId>
			<artifactId>angularjs</artifactId>
			<version>1.5.8</version>
		</dependency>
```

Then in `src/main/resource/static` add the following `index.html` page:

```html
<!DOCTYPE html>
<html lang="en" ng-app="greetingsApp">
<head>
    <meta charset="UTF-8">
    <title>Greetings</title>
    <script type="application/javascript" src="webjars/angularjs/1.5.8/angular.min.js"></script>
</head>
<body>
<div ng-controller="GreetingsController as greetings">
    <label>Name:</label>
    <input type="text" ng-model="greetings.name" placeholder="Enter a name here">
    <input type="button" value="Say hello" ng-click="greetings.sayHello()" />
    <hr>
    <h1>Hello {{greetings.name}}!</h1>
    <h1>{{greetings.message}}!</h1>
</div>

<script type="application/javascript">
    angular.module('greetingsApp', [])
        .controller('GreetingsController', function($http) {
            console.log("test");
            var greetings = this;
            greetings.sayHello = function() {
                console.log("say hello to " + greetings.name);
                $http({
                    url: "/greetings",
                    method: "GET",
                    params: {name: greetings.name}
                }).success(function(data) {
                    console.log(data);
                    greetings.message = data;
                });
            };
        });
</script>
</body>
</html>
```

### Testing

#### Maven setup

In the `pom.xml`, add the following dependency:

```xml
        <dependency>
			<groupId>org.seleniumhq.selenium</groupId>
			<artifactId>selenium-server</artifactId>
            <version>3.0.1</version>
            <scope>test</scope>
		</dependency>
```

A look to the [selenium documentation](http://www.seleniumhq.org/download/maven.jsp) can help to understand the dependency tree

#### The test class

Behind the scene this use the w3c standard [WebDriver](https://w3c.github.io/webdriver/webdriver-spec.html).
So for each driver you should provide some path to the driver. Those drivers can be downloaded from [Selenium - download](http://www.seleniumhq.org/download/).
Some version can be absent (64bits for example) but you can go directly to each driver specific site to get a version.
You can give a look to this [post](http://learn-automation.com/use-firefox-selenium-using-geckodriver-selenium-3/) to see in it action for Firefox.

In our case, will download the chromedriver achieve and unzip in the directory **4_auto_test_functional**. 

Next to the class `GreetingsApplicationTests`, create the new test class `GreetingsApplicationUiTests`:

```java
public class GreetingsApplicationUiTests {

    @BeforeClass
    public static void defineDrivers() {
        System.setProperty("webdriver.chrome.driver","../chromedriver"); // may be chromediver.exe if on windows
    }

    @Test
    public void test() throws Exception {
        WebDriver driver = new ChromeDriver();
        try {
            driver.get("http://localhost:8080");
            assertEquals("Greetings", driver.getTitle());
            driver.findElement(By.cssSelector("input[type=text]")).sendKeys("Richard");
            driver.findElement(By.cssSelector("input[type=button")).click();
            assertEquals("Hello Richard!", driver.findElement(By.cssSelector("h1")).getText());
        } finally {
            driver.close();
        }
    }

}
```

Run it to see it succeed.

### Moving forward

Use the [PageObject](http://docs.seleniumhq.org/docs/06_test_design_considerations.jsp#page-object-design-pattern) pattern to refactor the test.
This will typically illustrate how we can decouple the **what?** from the **how?** and so improve the test maintenance.

In our very simple case we may think of a PageObject **`GreetingsPage`** with three methods:

* `static GreetingsPage get(WebDriver driver, String baseUrl)`
* `void sayHello(String name)` which will fill the form then click on the button.
* `String getHelloMessage()` which will return the hello message.

So that finally our test could be re-written like this:

```java
    @Test
    public void test() throws MalformedURLException {
        WebDriver driver = new ChromeDriver();
        try {
            GreetingsPage page = GreetingsPage.get(driver, "http://localhost:8080");
            page.sayHello("Richard");
            assertEquals("Hello Richard!", page.getHelloMessage());
        } finally {
            driver.quit();
        }
    }
```

Once achieve this, we may refactor the html to define ids and so change `By.cssSelector` into `By.id()`.
This is in fact necessary because `By.cssSelector("h1")` is error prone since there's two elements for this selector.

## ui testing conclusion

In real application, we have to deal with many issues such as:

* Technical:
  - browser process management (sometimes when tests fail browser remain opens => process remain alive in the OS)
  - managing browser capabilities (SSL, proxy, ...)
  - headless browser when test occure on Linux jenkins slave with no GUI available
  - ...
* User Xp
  - UI is volatile (can really change a lot) => test maintenance should not be underestimated.

So to conclude, there's tons of projects which do not automate the UI test. 
Developpers automate the test of UI components such as controllers and models (in java or javascript) but not the screens.
This is also valid for BDD, which is our next stop. 