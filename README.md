valuable articles:

* [http://stephenwalther.com/archive/2009/04/11/tdd-tests-are-not-unit-tests](http://stephenwalther.com/archive/2009/04/11/tdd-tests-are-not-unit-tests)
* [https://reinhard.codes/2015/12/27/software-testing-good-bad-ugly/](https://reinhard.codes/2015/12/27/software-testing-good-bad-ugly/)
* [https://blog.codecentric.de/en/2015/10/continuous-integration-platform-using-docker-container-jenkins-sonarqube-nexus-gitlab/](https://blog.codecentric.de/en/2015/10/continuous-integration-platform-using-docker-container-jenkins-sonarqube-nexus-gitlab/)
* [https://github.com/harbur/docker-sdlc](https://github.com/harbur/docker-sdlc)
* [http://agiledata.org/essays/tdd.html](http://agiledata.org/essays/tdd.html)
